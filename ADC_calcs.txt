ADC Calcs
---------

12 bit ADC, conversion rate of 200kHz

Vref+ & Vdda are tied to filtered 3.3v on the Mini-DK2

Max voltage resolution = 3.3/2^12 = 0.000805664v = 0.805mV per bit

If a current shunt resistor of value 0.01 Ohm is used:
Minimum measureable current = 0.000805664 / 0.05 * Amp Gain(20)
                            = 0.00402832 A (4.02mA)
Maximum measureable current = 3.3 / 0.05 * Amp Gain(20)
                            = 3.3A

This gives a resolution of close enough to 1mA (when rounded).

Resistor: (0.05Ohm, 1W)
http://au.element14.com/susumu/prl1632-r050-f-t5/resistor-current-sense-0-05-ohm/dp/1268782

Current sense amp: (250kHz, Gain = 20)
http://au.element14.com/maxim-integrated-products/max4080tasa/current-sense-amp-250khz-8soic/dp/1593368


Power disipation in resistor (W) = V * I
                                 = I^2 . R
                                 = 0.5445W (within tolerance)
